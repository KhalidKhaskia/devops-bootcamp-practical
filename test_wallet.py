"""Docstring"""
import pytest
from wallet import Wallet, InsufficientAmount


def test_default_initial_amount():
    """Docstring"""
    wallet = Wallet()
    assert wallet.balance == 0

def test_setting_initial_amount():
    """Docstring"""
    wallet = Wallet(100)
    assert wallet.balance == 100

def test_wallet_add_cash():
    """Docstring"""
    wallet = Wallet(10)
    wallet.add_cash(90)
    assert wallet.balance == 100

def test_wallet_spend_cash():
    """Docstring"""
    wallet = Wallet(20)
    wallet.spend_cash(10)
    assert wallet.balance == 10

def test_wallet_spend_cash_raises_exception_on_insufficient_amount():
    """Docstring"""
    wallet = Wallet()
    with pytest.raises(InsufficientAmount):
        wallet.spend_cash(100)
