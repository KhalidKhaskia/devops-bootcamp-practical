"""Docstring"""
class InsufficientAmount(Exception):
    """Docstring"""

class Wallet():
    """Docstring"""
    def __init__(self, initial_amount=0):
        """Docstring"""
        self.balance = initial_amount

    def spend_cash(self, amount):
        """Docstring"""
        if self.balance < amount:
            raise InsufficientAmount('Not enough available to spend {}'.format(amount))
        self.balance -= amount

    def add_cash(self, amount):
        """Docstring"""
        self.balance += amount
